package DatabaseConnector;

use DBI;
use strict;
use warnings;

use Designer;
use Formatter;

sub new{
  my($object, $attributes) = @_;
  my $this = bless {
    serverAddress => $attributes->{serverAddress},
    username => $attributes->{username},
    password => $attributes->{password},
    export => $attributes->{export},
    constellation => $attributes->{constellation}    
  }, $object;

  return $this;
}

sub exportConstellation{

  my $this = shift();

  my $documentDesigner = Designer->new();
  my $documentFormatter = Formatter->new();

  my $databaseConnection = DBI->connect(
    $this->{serverAddress},
    $this->{username},
    $this->{password}
  );

  my $export;
  my $execution;
  my $queryParameters;
  my $databaseStatement;
  my $fileFlag;
  my $fileName;
  my $fileHandle;
  my $fileType;
  my $latinDesignation;
  my $tableColumns;

  my @stellarParameters; 

  $databaseConnection->{'mysql_enable_utf8'} = 1;
  $databaseConnection->do('SET NAMES utf8');

  $export = $this->{export};
  $latinDesignation = $this->{constellation};

  $fileType = ".html";

  if($export eq "-f"){
  $queryParameters = "SELECT *
    FROM stellarParameters
    WHERE designation
    LIKE '%" . $latinDesignation . "'";
  $fileFlag = " - Full";
  } elsif($export eq "-t"){
    $queryParameters = "SELECT *
    FROM stellarParadesTransient
    WHERE designation
    LIKE '%" . $latinDesignation . "'";
    $fileFlag = " - Trim";
  }

  $databaseStatement = $databaseConnection->prepare($queryParameters);
  $execution = $databaseStatement->execute();

  unless($execution == 0){
    
    $fileName = $latinDesignation . $fileFlag . $fileType;

    open $fileHandle, ">:utf8", $fileName
        or die "could not open $fileName...\n";
        
    print $fileHandle $documentFormatter->{openHtml};
    print $fileHandle $documentFormatter->{openHead};
    print $fileHandle $documentFormatter->{openStyle};

    print $fileHandle $documentDesigner->{indentSelector} . "body" . $documentDesigner->{openBlock};
    print $fileHandle $documentDesigner->{indentBlock}. "background-color: " . $documentDesigner->{backgroundColor};
    print $fileHandle $documentDesigner->{indentBlock}. "font-family: " . $documentDesigner->{fontFamily};
    print $fileHandle $documentDesigner->{closeBlock};

    print $fileHandle $documentDesigner->{indentSelector} . "th" . $documentDesigner->{openBlock};
    print $fileHandle $documentDesigner->{indentBlock}. "padding: " . $documentDesigner->{padding};
    print $fileHandle $documentDesigner->{indentBlock}. "font-weight: " . $documentDesigner->{fontWeight};
    print $fileHandle $documentDesigner->{closeBlock};

    print $fileHandle $documentDesigner->{indentSelector} . "td" . $documentDesigner->{openBlock};
    print $fileHandle $documentDesigner->{indentBlock}. "padding: " . $documentDesigner->{padding};
    print $fileHandle $documentDesigner->{closeBlock};

    print $fileHandle $documentDesigner->{indentSelector} . "table" . $documentDesigner->{openBlock};
    print $fileHandle $documentDesigner->{indentBlock}. "table-layout: " . $documentDesigner->{tableLayout};
    print $fileHandle $documentDesigner->{closeBlock};

    print $fileHandle $documentDesigner->{indentSelector} . "th, " . "td, " . "table" . $documentDesigner->{openBlock};
    print $fileHandle $documentDesigner->{indentBlock}. "border-style: " . $documentDesigner->{borderStyle};
    print $fileHandle $documentDesigner->{indentBlock}. "border-width: " . $documentDesigner->{borderWidth};
    print $fileHandle $documentDesigner->{indentBlock}. "border-collapse: " . $documentDesigner->{borderCollapse};
    print $fileHandle $documentDesigner->{closeBlock};

    print $fileHandle $documentFormatter->{closeStyle};
    print $fileHandle $documentFormatter->{closeHead};
    print $fileHandle $documentFormatter->{openBody};
    print $fileHandle $documentFormatter->{openTable};
  
    $tableColumns = $databaseStatement->{NAME};

    print $fileHandle $documentFormatter->{openTableRow};
    foreach(@{$tableColumns}){
      print $fileHandle $documentFormatter->{openTableHeader};
      print $fileHandle $_;
      print $fileHandle $documentFormatter->{closeTableHeader};
    }
    print $fileHandle $documentFormatter->{closeTableRow};

    while(@stellarParameters = $databaseStatement->fetchrow_array()){
            
      print $fileHandle $documentFormatter->{openTableRow};
      foreach(@stellarParameters){

        print $fileHandle $documentFormatter->{openTableCell};        
        unless(defined($_) == 0) { print $fileHandle $_; } else {
          $_ = "";
          print $fileHandle $_; 
        }
        print $fileHandle $documentFormatter->{closeTableCell};
      }
      print $fileHandle $documentFormatter->{closeTableRow};
    }

    print $fileHandle $documentFormatter->{closeTable};
    print $fileHandle $documentFormatter->{closeBody};
    print $fileHandle $documentFormatter->{closeHtml};
  }  

  $databaseStatement->finish();
  $databaseConnection->disconnect();
}

1;
